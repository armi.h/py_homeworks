from random import randint


def guess_number():
    random_num = randint(0, 10)
    flag = True

    while random_num and flag:
        user_input = input("Enter any number. -> ")

        if not user_input.isnumeric():
            print("Please enter a number.")
        elif random_num > int(user_input):
            print("It’s less than needed.")
        elif random_num < int(user_input):
            print("It’s bigger than needed.")
        else:
            print('Great! Victory!')
            answer = input("Would you like to play again? Yes/No -> ")

            if answer.lower() == "no":
                flag = False
                print("Game Over! Bye!")


guess_number()
