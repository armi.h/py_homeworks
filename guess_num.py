from random import randint

random_num = randint(0, 10)
next_step = True

while random_num and next_step:
    user_input = input("Enter any number. -> ")

    if not user_input.isnumeric():
        print("Please enter a number.")
    elif random_num > int(user_input):
        print("It’s less than needed.")
    elif random_num < int(user_input):
        print("It’s bigger than needed.")
    elif random_num == int(user_input):
        print("Great! Victory!")
        answer = input("Would you like to play again? Yes/No -> ")

        if answer.lower() == "no":
            print("Game Over! Bye!")
            next_step = False
    else:
        print("Oop! Something is wrong!")

