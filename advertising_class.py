from random import choice


class Advertising:

    def __init__(self, ad_type, duration=10, daily_repetition=3, day_time=None, lines=7):
        self.type = ad_type
        self.duration = duration
        self.daily_repetition = daily_repetition
        self.times_of_day = day_time
        self.lines = lines
        self.price = 0

    def media_price(self):
        """
        Takes in a ad parameters for counting and returns the total price
            Parameters:
                self.type (str): string
                self.times_of_day (str): string
                self.duration (int): integer
                self.daily_repetition (int): integer
            Returns:
                self.price (int): integer
        """

        if self.type == 'tv':
            self.price = 150
        elif self.type == 'online':
            self.price = 200
        else:
            self.price = 100

        if self.times_of_day == 'morning':
            self.price = self.price + 80
        elif self.times_of_day == 'afternoon':
            self.price = self.price + 60
        elif self.times_of_day == 'evening':
            self.price = self.price + 100
        else:
            self.price = self.price + choice((80, 60, 100))

        return self.price + 20 * self.duration + 50 * self.daily_repetition

    def newspaper_price(self):
        """
        Takes in a ad parameters for counting and returns the total price
            Parameters:
                self.type (str): string
                self.duration (int): integer
                self.lines (int): integer
            Returns:
                self.price (int): integer
        """

        if self.type == 'newspaper':
            self.price = 100
        elif self.type == 'magazine':
            self.price = 110
        else:
            self.price = 80

        return self.price + 25 * self.duration + 10 * self.lines


globbing_ad = Advertising(ad_type='tv', duration=3, daily_repetition=2, day_time='evening')
print(f" Total price =", globbing_ad.media_price(), "USD")

mts_armenia = Advertising(ad_type='newspaper', duration=4, lines=10)
print(f" Total price =", mts_armenia.newspaper_price(), "USD")
