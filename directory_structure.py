import os


def find_path(root_path):
    directories = os.listdir(root_path)
    counter = 1

    for directory in directories:
        indent = '-' * counter
        print(f"{indent} {directory}")

        if os.path.isdir(root_path + "/" + directory):
            nested_path = root_path + "/" + directory
            find_path(nested_path)
        counter = counter + 1


path = "D:/Downloads"
find_path(path)
